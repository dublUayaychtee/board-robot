from time import sleep
import random
import traceback
import os

class Robot:
    def __init__(self, w = 10, h = 10, brd = None, loc = [], delay = 0.25):
        """
        w = Width of board
        h = Height of board
        brd = Preset board contents, board w and h will be set automatically
        loc = Starting location. (Top left is 0, 0) [x, y]
        delay = Amount of time between every move
        """

        self.history = []

        os.system('cls' if os.name == 'nt' else 'clear')
        self.w = w
        self.h = h
        if not brd:
            self.brd = [[0 for i in range(w)] for i in range(h)]
        else:
            self.brd = brd
            self.h = len(brd)
            self.w = len(brd[0])
            width = len(brd[0])
            for i in brd:
                if len(i) != width:
                    raise ValueError('Board Width is not consistent')
        self.loc = loc
        if not loc:
            self.loc = [w // 2, h // 2]
        self.delay = delay
        self.messages = []
        self.print()

        """
        for y1 in range(y):
            brd.append([])
            for x1 in range(x):
                brd[y].append(0)
        """

    def generate(self, w, h, seed = None, delay = True):
        """
        w = Width
        h = Height
        """

        random.seed(seed)

        self.w = w
        self.h = h
        self.brd = [[1 for _ in range(w)] for _ in range(h)]
        self.loc = [0, 0]
        self.print()

        directions = {
            0: [0, -1],
            1: [1, 0],
            2: [0, 1],
            3: [-1, 0],
        }

        history = [[0, 0]]
        goal = None

        done = False
        while len(history) > 0:
            self.loc = history[-1:][0][:]
            self.brd[self.loc[1]][self.loc[0]] = 0
            self.print()
            
            validDirections = []
            for k, v in directions.items():
                if 0 <= self.loc[0] + v[0]*2 < self.w \
                 and 0 <= self.loc[1] + v[1]*2 < self.h:
                    if self.brd[self.loc[1] + v[1]*2][self.loc[0] + v[0]*2] == 1:
                        validDirections.append(k)
            
            if len(validDirections) > 0:
                direction = directions[random.choice(validDirections)]
                self.brd[self.loc[1] + direction[1]][self.loc[0] + direction[0]] = 0
                self.loc[0] += direction[0] * 2
                self.loc[1] += direction[1] * 2
                history.append(self.loc[:])
            else:
                if goal == None:
                    goal = history.pop(-1)
                else:
                    history.pop(-1)
            if delay:
                sleep(self.delay / 8)
        
        self.brd[goal[1]][goal[0]] = 2
        self.print()

    def isOutOfBounds(self, x, y):
        """
        check if a point is out of bounds
        """
        if x < 0 or x >= self.w \
        or y < 0 or y >= self.h:
            return True
        return False

    def move(self, x = None, y = None):
        """
        X and Y can either be True to add one, False to subtract one, and None to do nothing.
        """

        if x is not None:
            if y is not None:
                self.messages.append("Can't move diagonally!")
                return

        # get new location
        newX = self.loc[0] + (1 if x else 0 if x is None else -1)
        newY = self.loc[1] + (1 if y else 0 if y is None else -1)

        if not self.isOutOfBounds(newX, newY) \
           and self.brd[newY][newX] != 1:
            # save new location
            self.loc[0] = newX
            self.loc[1] = newY
            if self.brd[newY][newX] == 2:
                self.messages.append("You made it!")
            self.print() # update screen
            sleep(self.delay)
            return self

        # collision check on new location
        self.messages.append("Couldn't move there!")
        self.print()

    def up(self, n = 1):
        if n < 0: return
        for _ in range(n):
            self.move(y = False)
        return self
            
    def down(self, n = 1):
        if n < 0: return
        for _ in range(n):
            self.move(y = True)
        return self

    def left(self, n = 1):
        if n < 0: return
        for _ in range(n):
            self.move(x = False)
        return self

    def right(self, n = 1):
        if n < 0: return
        for _ in range(n):
            self.move(x = True)
        return self

    def idToString(self, i):
        if i == 0: return '.'
        if i == 1: return '#'
        if i == 2: return '~'
    
    def print(self):
        print('\033[2J\033[H', end = '') # clear and move to top left code

        for i in self.brd:
            print(' '.join(map(self.idToString, i)))

        print("\033[%d;%dH" % (self.loc[1] + 1, self.loc[0] * 2 + 1) + "@") # move to robot position and write "@"
        print("\033[%d;0H" % (self.h + 1)) # move cursor off of the board
        print("Location: " + str(self.loc))
        print('\n'.join(self.messages[-5:]))

        return self

    def prompt(self):
        self.print()
        contents = []
        print("Press Enter on an empty line to run code")
        while True:
            line = input("> ")
            if line == "":
                break
            contents.append(line)

        code = '\n'.join(contents)
        try:
            exec(code)
            self.history += contents
            return code
        except Exception:
            print(traceback.format_exc())

if __name__ == "__main__":
    robot = Robot()
    robot.generate(20, 20)
    
    while True:
        robot.prompt()
